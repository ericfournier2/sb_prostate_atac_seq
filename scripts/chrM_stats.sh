#!/bin/bash
module load samtools
mugqic_dir=output/pipeline

# Loop over all readset alignments
for i in $mugqic_dir/alignment/*
do
    lib_name=`basename $i`
    bam_name=$i/*/*.bam
    total_aligned=`samtools idxstats $bam_name | cut -f 3 | awk '{s+=$1} END {print s}'`
    total_unaligned=`samtools idxstats $bam_name | cut -f 4 | awk '{s+=$1} END {print s}'`
    total_chrM=`samtools idxstats $bam_name | grep -F 'chrM' | cut -f 3`
    
    echo $lib_name $total_aligned $total_unaligned $total_chrM
done    

# Loop over all readset alignments
for i in $mugqic_dir/alignment/*
do
    lib_name=`basename $i`
    bam_name=$i/*.sorted.dup.bam
    total_aligned=`samtools idxstats $bam_name | cut -f 3 | awk '{s+=$1} END {print s}'`
    total_unaligned=`samtools idxstats $bam_name | cut -f 4 | awk '{s+=$1} END {print s}'`
    total_chrM=`samtools idxstats $bam_name | grep -F 'chrM' | cut -f 3`
    
    echo $lib_name $total_aligned $total_unaligned $total_chrM
done    
