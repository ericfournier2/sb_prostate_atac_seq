#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-07-11T16:30:12
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 9 jobs
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 10 jobs
#   samtools_view_filter: 10 jobs
#   picard_merge_sam_files: 9 jobs
#   picard_mark_duplicates: 10 jobs
#   metrics: 2 jobs
#   homer_make_tag_directory: 9 jobs
#   qc_metrics: 1 job
#   homer_make_ucsc_file: 19 jobs
#   macs2_callpeak: 19 jobs
#   TOTAL: 99 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/scratch/efournie/ATAC-seq/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.10d736ba129d3f4ca2bb6a2115c75124.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.10d736ba129d3f4ca2bb6a2115c75124.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P01-OCT-H && \
`cat > trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.10d736ba129d3f4ca2bb6a2115c75124.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.bd22af8f1f0b50d0fb4300a18f81ffa8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.bd22af8f1f0b50d0fb4300a18f81ffa8.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P01-SF-C && \
`cat > trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.bd22af8f1f0b50d0fb4300a18f81ffa8.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.1d8131d7fb4dba44a0a00b3667710f4f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.1d8131d7fb4dba44a0a00b3667710f4f.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P056-FRZ6-5p && \
`cat > trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.1d8131d7fb4dba44a0a00b3667710f4f.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc549cc565a8c05edc734109e1d1946a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc549cc565a8c05edc734109e1d1946a.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P059-FRZ5-5p && \
`cat > trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc549cc565a8c05edc734109e1d1946a.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c25a9564d34ae169feef0ea8a6464081.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c25a9564d34ae169feef0ea8a6464081.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P62-FRZ3-3p && \
`cat > trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c25a9564d34ae169feef0ea8a6464081.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.620736af61d526a9cc10c798871dd1a5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.620736af61d526a9cc10c798871dd1a5.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P081-FRZ5-3p && \
`cat > trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.620736af61d526a9cc10c798871dd1a5.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_7_JOB_ID: trimmomatic.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c9d8149abf772ff4d9ecd69a0328fbe6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c9d8149abf772ff4d9ecd69a0328fbe6.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P092-FRZ3-4p && \
`cat > trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c9d8149abf772ff4d9ecd69a0328fbe6.mugqic.done
)
trimmomatic_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_8_JOB_ID: trimmomatic.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b75290df533ab23e9cb9397abdbe7bfc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b75290df533ab23e9cb9397abdbe7bfc.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/P096-FRZ7-4p && \
`cat > trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b75290df533ab23e9cb9397abdbe7bfc.mugqic.done
)
trimmomatic_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: trimmomatic_9_JOB_ID: trimmomatic.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.839754a97522754406fdad7ab08e3f48.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.839754a97522754406fdad7ab08e3f48.mugqic.done'
module load java/1.8.0_121 mugqic/trimmomatic/0.36 && \
mkdir -p trim/PZ-HPV7 && \
`cat > trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa << END
>Single
TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 1 \
  -phred33 \
  /scratch/efournie/ATAC-seq/raw/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz \
  trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz \
  ILLUMINACLIP:trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log
trimmomatic.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.839754a97522754406fdad7ab08e3f48.mugqic.done
)
trimmomatic_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"trimmomatic\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID:$trimmomatic_7_JOB_ID:$trimmomatic_8_JOB_ID:$trimmomatic_9_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.8f5aa92a7c6da6beddc8ed31ff14bf00.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.8f5aa92a7c6da6beddc8ed31ff14bf00.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P01-OCT-H	HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P01-SF-C	HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P056-FRZ6-5p	HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P059-FRZ5-5p	HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P62-FRZ3-3p	HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P081-FRZ5-3p	HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P092-FRZ3-4p	HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/P096-FRZ7-4p	HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/PZ-HPV7	HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /home/efournie/genpipes/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.8f5aa92a7c6da6beddc8ed31ff14bf00.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"merge_trimmomatic_stats\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c504394b8fb367185a0b556a99679937.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c504394b8fb367185a0b556a99679937.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P01-OCT-H	LB:P01-OCT-H	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.c504394b8fb367185a0b556a99679937.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b0d0535f65e6cc3487787c31110f21d6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b0d0535f65e6cc3487787c31110f21d6.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P01-SF-C	LB:P01-SF-C	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.b0d0535f65e6cc3487787c31110f21d6.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_3_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.399dd8b70de7eb343403ede419c7ab6c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.399dd8b70de7eb343403ede419c7ab6c.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P056-FRZ6-5p	LB:P056-FRZ6-5p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.399dd8b70de7eb343403ede419c7ab6c.mugqic.done
)
bwa_mem_picard_sort_sam_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_4_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.2388c5fc6b4cf3c84c821611a74c6444.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.2388c5fc6b4cf3c84c821611a74c6444.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P059-FRZ5-5p	LB:P059-FRZ5-5p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.2388c5fc6b4cf3c84c821611a74c6444.mugqic.done
)
bwa_mem_picard_sort_sam_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_5_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7f48b2ad6d7964f069f4fdf9a343cb6c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7f48b2ad6d7964f069f4fdf9a343cb6c.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P62-FRZ3-3p	LB:P62-FRZ3-3p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7f48b2ad6d7964f069f4fdf9a343cb6c.mugqic.done
)
bwa_mem_picard_sort_sam_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_6_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7da7619d7a2f23191213e5b9def42140.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7da7619d7a2f23191213e5b9def42140.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P081-FRZ5-3p	LB:P081-FRZ5-3p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.7da7619d7a2f23191213e5b9def42140.mugqic.done
)
bwa_mem_picard_sort_sam_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_7_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.24f676b7a45376cc2291924e873f11a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.24f676b7a45376cc2291924e873f11a6.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P092-FRZ3-4p	LB:P092-FRZ3-4p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.24f676b7a45376cc2291924e873f11a6.mugqic.done
)
bwa_mem_picard_sort_sam_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_8_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.ba0b315267301708551c75b4235dc698.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.ba0b315267301708551c75b4235dc698.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:P096-FRZ7-4p	LB:P096-FRZ7-4p	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.ba0b315267301708551c75b4235dc698.mugqic.done
)
bwa_mem_picard_sort_sam_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_9_JOB_ID: bwa_mem_picard_sort_sam.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.e52182283b3b4417f967d4a3ea9e8452.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.e52182283b3b4417f967d4a3ea9e8452.mugqic.done'
module load mugqic/bwa/0.7.12 java/1.8.0_121 mugqic/picard/2.0.1 && \
mkdir -p alignment/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz	SM:PZ-HPV7	LB:PZ-HPV7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/bwa_index/Homo_sapiens.GRCh38.fa \
  trim/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.trim.single.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.e52182283b3b4417f967d4a3ea9e8452.mugqic.done
)
bwa_mem_picard_sort_sam_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"bwa_mem_picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_10_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID:$bwa_mem_picard_sort_sam_2_JOB_ID:$bwa_mem_picard_sort_sam_3_JOB_ID:$bwa_mem_picard_sort_sam_4_JOB_ID:$bwa_mem_picard_sort_sam_5_JOB_ID:$bwa_mem_picard_sort_sam_6_JOB_ID:$bwa_mem_picard_sort_sam_7_JOB_ID:$bwa_mem_picard_sort_sam_8_JOB_ID:$bwa_mem_picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.22d7ff6198a068bd5763589f2515f7f6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.22d7ff6198a068bd5763589f2515f7f6.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Homo_sapiens" \
  --variable assembly="GRCh38" \
  /home/efournie/genpipes/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.22d7ff6198a068bd5763589f2515f7f6.mugqic.done
)
bwa_mem_picard_sort_sam_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4d0ec3371c2fea64ebe1278c66d4e491.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4d0ec3371c2fea64ebe1278c66d4e491.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P01-OCT-H/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4d0ec3371c2fea64ebe1278c66d4e491.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.aad8cf6c189e9e45719cf36a16ab4643.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.aad8cf6c189e9e45719cf36a16ab4643.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P01-SF-C/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.aad8cf6c189e9e45719cf36a16ab4643.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.105e52ee24280b88e356c713693969ee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.105e52ee24280b88e356c713693969ee.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P056-FRZ6-5p/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.105e52ee24280b88e356c713693969ee.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc9c79fa864bda5369c0758c82fc9236.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc9c79fa864bda5369c0758c82fc9236.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P059-FRZ5-5p/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.dc9c79fa864bda5369c0758c82fc9236.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_5_JOB_ID: samtools_view_filter.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.cca7688043495555ea4f58693582b812.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.cca7688043495555ea4f58693582b812.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P62-FRZ3-3p/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.cca7688043495555ea4f58693582b812.mugqic.done
)
samtools_view_filter_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_6_JOB_ID: samtools_view_filter.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.a3ac9ceca7433838d55fbd497f529e8b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.a3ac9ceca7433838d55fbd497f529e8b.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P081-FRZ5-3p/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.a3ac9ceca7433838d55fbd497f529e8b.mugqic.done
)
samtools_view_filter_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_7_JOB_ID: samtools_view_filter.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.39640c391a644716c76c20e56202459e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.39640c391a644716c76c20e56202459e.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P092-FRZ3-4p/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.39640c391a644716c76c20e56202459e.mugqic.done
)
samtools_view_filter_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_8_JOB_ID: samtools_view_filter.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.3b68c9466159fb427c23604b66ad7e88.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.3b68c9466159fb427c23604b66ad7e88.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/P096-FRZ7-4p/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.3b68c9466159fb427c23604b66ad7e88.mugqic.done
)
samtools_view_filter_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_9_JOB_ID: samtools_view_filter.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4ba28e58e3780db43fe58f453db32a41.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4ba28e58e3780db43fe58f453db32a41.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.bam \
  > alignment/PZ-HPV7/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam
samtools_view_filter.HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.4ba28e58e3780db43fe58f453db32a41.mugqic.done
)
samtools_view_filter_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"samtools_view_filter\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_10_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID:$samtools_view_filter_4_JOB_ID:$samtools_view_filter_5_JOB_ID:$samtools_view_filter_6_JOB_ID:$samtools_view_filter_7_JOB_ID:$samtools_view_filter_8_JOB_ID:$samtools_view_filter_9_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /home/efournie/genpipes/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.adda516c06f797351611d7331668a33d.mugqic.done
)
samtools_view_filter_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P01-OCT-H
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P01-OCT-H.cd552856ffe1999e7796db4fb2dd7a07.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P01-OCT-H.cd552856ffe1999e7796db4fb2dd7a07.mugqic.done'
mkdir -p alignment/P01-OCT-H && \
ln -s -f HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N701---N517.P01-OCT-H_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P01-OCT-H/P01-OCT-H.merged.bam
symlink_readset_sample_bam.P01-OCT-H.cd552856ffe1999e7796db4fb2dd7a07.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P01-SF-C
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P01-SF-C.bd5f46c5f455b891b01c0c492b284815.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P01-SF-C.bd5f46c5f455b891b01c0c492b284815.mugqic.done'
mkdir -p alignment/P01-SF-C && \
ln -s -f HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N702---N517.P01-SF-C_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P01-SF-C/P01-SF-C.merged.bam
symlink_readset_sample_bam.P01-SF-C.bd5f46c5f455b891b01c0c492b284815.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P056-FRZ6-5p
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P056-FRZ6-5p.4d1971f54830ffe9026ca8f952bf569b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P056-FRZ6-5p.4d1971f54830ffe9026ca8f952bf569b.mugqic.done'
mkdir -p alignment/P056-FRZ6-5p && \
ln -s -f HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N703---N517.P056-FRZ6-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P056-FRZ6-5p/P056-FRZ6-5p.merged.bam
symlink_readset_sample_bam.P056-FRZ6-5p.4d1971f54830ffe9026ca8f952bf569b.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_4_JOB_ID: symlink_readset_sample_bam.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P059-FRZ5-5p
JOB_DEPENDENCIES=$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P059-FRZ5-5p.a9469dd12a4c95f28f2166a57c45445d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P059-FRZ5-5p.a9469dd12a4c95f28f2166a57c45445d.mugqic.done'
mkdir -p alignment/P059-FRZ5-5p && \
ln -s -f HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N704---N517.P059-FRZ5-5p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P059-FRZ5-5p/P059-FRZ5-5p.merged.bam
symlink_readset_sample_bam.P059-FRZ5-5p.a9469dd12a4c95f28f2166a57c45445d.mugqic.done
)
picard_merge_sam_files_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_5_JOB_ID: symlink_readset_sample_bam.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P62-FRZ3-3p
JOB_DEPENDENCIES=$samtools_view_filter_5_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P62-FRZ3-3p.70186ad8a865da21f9b971c74048428f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P62-FRZ3-3p.70186ad8a865da21f9b971c74048428f.mugqic.done'
mkdir -p alignment/P62-FRZ3-3p && \
ln -s -f HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N705---N517.P62-FRZ3-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P62-FRZ3-3p/P62-FRZ3-3p.merged.bam
symlink_readset_sample_bam.P62-FRZ3-3p.70186ad8a865da21f9b971c74048428f.mugqic.done
)
picard_merge_sam_files_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_6_JOB_ID: symlink_readset_sample_bam.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P081-FRZ5-3p
JOB_DEPENDENCIES=$samtools_view_filter_6_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P081-FRZ5-3p.b0790a1bd40313990b09dfafa125ea63.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P081-FRZ5-3p.b0790a1bd40313990b09dfafa125ea63.mugqic.done'
mkdir -p alignment/P081-FRZ5-3p && \
ln -s -f HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N706---N517.P081-FRZ5-3p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P081-FRZ5-3p/P081-FRZ5-3p.merged.bam
symlink_readset_sample_bam.P081-FRZ5-3p.b0790a1bd40313990b09dfafa125ea63.mugqic.done
)
picard_merge_sam_files_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_7_JOB_ID: symlink_readset_sample_bam.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P092-FRZ3-4p
JOB_DEPENDENCIES=$samtools_view_filter_7_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P092-FRZ3-4p.0038abfec2014598d64b7e0375e85bbe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P092-FRZ3-4p.0038abfec2014598d64b7e0375e85bbe.mugqic.done'
mkdir -p alignment/P092-FRZ3-4p && \
ln -s -f HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N707---N517.P092-FRZ3-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P092-FRZ3-4p/P092-FRZ3-4p.merged.bam
symlink_readset_sample_bam.P092-FRZ3-4p.0038abfec2014598d64b7e0375e85bbe.mugqic.done
)
picard_merge_sam_files_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_8_JOB_ID: symlink_readset_sample_bam.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.P096-FRZ7-4p
JOB_DEPENDENCIES=$samtools_view_filter_8_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.P096-FRZ7-4p.4b82cd15a6c54b871b82111b4569de5a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.P096-FRZ7-4p.4b82cd15a6c54b871b82111b4569de5a.mugqic.done'
mkdir -p alignment/P096-FRZ7-4p && \
ln -s -f HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N708---N517.P096-FRZ7-4p_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/P096-FRZ7-4p/P096-FRZ7-4p.merged.bam
symlink_readset_sample_bam.P096-FRZ7-4p.4b82cd15a6c54b871b82111b4569de5a.mugqic.done
)
picard_merge_sam_files_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_9_JOB_ID: symlink_readset_sample_bam.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.PZ-HPV7
JOB_DEPENDENCIES=$samtools_view_filter_9_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.PZ-HPV7.c3ef1d8611e16ce62b67213513b466fa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.PZ-HPV7.c3ef1d8611e16ce62b67213513b466fa.mugqic.done'
mkdir -p alignment/PZ-HPV7 && \
ln -s -f HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz/HI.4762.005.N709---N517.PZ-HPV7_ATAC-SEQ_NA_NA_NA_Rep1_R1.fastq.gz.sorted.filtered.bam alignment/PZ-HPV7/PZ-HPV7.merged.bam
symlink_readset_sample_bam.PZ-HPV7.c3ef1d8611e16ce62b67213513b466fa.mugqic.done
)
picard_merge_sam_files_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P01-OCT-H
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P01-OCT-H.c4df8cc04c7b9c6a39adbb15f07a1a03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P01-OCT-H.c4df8cc04c7b9c6a39adbb15f07a1a03.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P01-OCT-H/P01-OCT-H.merged.bam \
 OUTPUT=alignment/P01-OCT-H/P01-OCT-H.sorted.dup.bam \
 METRICS_FILE=alignment/P01-OCT-H/P01-OCT-H.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P01-OCT-H.c4df8cc04c7b9c6a39adbb15f07a1a03.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P01-SF-C
JOB_DEPENDENCIES=$picard_merge_sam_files_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P01-SF-C.ef1aa52e6f5ccc63ae5efaf1e0657d0d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P01-SF-C.ef1aa52e6f5ccc63ae5efaf1e0657d0d.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P01-SF-C/P01-SF-C.merged.bam \
 OUTPUT=alignment/P01-SF-C/P01-SF-C.sorted.dup.bam \
 METRICS_FILE=alignment/P01-SF-C/P01-SF-C.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P01-SF-C.ef1aa52e6f5ccc63ae5efaf1e0657d0d.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P056-FRZ6-5p
JOB_DEPENDENCIES=$picard_merge_sam_files_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P056-FRZ6-5p.a776cb00bd5f638fefc968cb53015e71.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P056-FRZ6-5p.a776cb00bd5f638fefc968cb53015e71.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P056-FRZ6-5p/P056-FRZ6-5p.merged.bam \
 OUTPUT=alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.bam \
 METRICS_FILE=alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P056-FRZ6-5p.a776cb00bd5f638fefc968cb53015e71.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P059-FRZ5-5p
JOB_DEPENDENCIES=$picard_merge_sam_files_4_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P059-FRZ5-5p.3ed89898e33f453e5f28b14e692149aa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P059-FRZ5-5p.3ed89898e33f453e5f28b14e692149aa.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P059-FRZ5-5p/P059-FRZ5-5p.merged.bam \
 OUTPUT=alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.bam \
 METRICS_FILE=alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P059-FRZ5-5p.3ed89898e33f453e5f28b14e692149aa.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P62-FRZ3-3p
JOB_DEPENDENCIES=$picard_merge_sam_files_5_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P62-FRZ3-3p.04cfada3416d33b84b4c95b7617b3151.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P62-FRZ3-3p.04cfada3416d33b84b4c95b7617b3151.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P62-FRZ3-3p/P62-FRZ3-3p.merged.bam \
 OUTPUT=alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.bam \
 METRICS_FILE=alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P62-FRZ3-3p.04cfada3416d33b84b4c95b7617b3151.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P081-FRZ5-3p
JOB_DEPENDENCIES=$picard_merge_sam_files_6_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P081-FRZ5-3p.7296f094167e5feb700b222ad0685f82.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P081-FRZ5-3p.7296f094167e5feb700b222ad0685f82.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P081-FRZ5-3p/P081-FRZ5-3p.merged.bam \
 OUTPUT=alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.bam \
 METRICS_FILE=alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P081-FRZ5-3p.7296f094167e5feb700b222ad0685f82.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P092-FRZ3-4p
JOB_DEPENDENCIES=$picard_merge_sam_files_7_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P092-FRZ3-4p.382b95c24801e762bebef2a17d69e8b9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P092-FRZ3-4p.382b95c24801e762bebef2a17d69e8b9.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P092-FRZ3-4p/P092-FRZ3-4p.merged.bam \
 OUTPUT=alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.bam \
 METRICS_FILE=alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P092-FRZ3-4p.382b95c24801e762bebef2a17d69e8b9.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.P096-FRZ7-4p
JOB_DEPENDENCIES=$picard_merge_sam_files_8_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.P096-FRZ7-4p.1921ba0130983d95ad825eea475ac807.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.P096-FRZ7-4p.1921ba0130983d95ad825eea475ac807.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/P096-FRZ7-4p/P096-FRZ7-4p.merged.bam \
 OUTPUT=alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.bam \
 METRICS_FILE=alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.P096-FRZ7-4p.1921ba0130983d95ad825eea475ac807.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.PZ-HPV7
JOB_DEPENDENCIES=$picard_merge_sam_files_9_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.PZ-HPV7.df986412a55a6aca5dd6aa028bfa4c90.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.PZ-HPV7.df986412a55a6aca5dd6aa028bfa4c90.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/PZ-HPV7/PZ-HPV7.merged.bam \
 OUTPUT=alignment/PZ-HPV7/PZ-HPV7.sorted.dup.bam \
 METRICS_FILE=alignment/PZ-HPV7/PZ-HPV7.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.PZ-HPV7.df986412a55a6aca5dd6aa028bfa4c90.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_10_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.8bfd4b03ed7ee119ffcd84a77e108045.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.8bfd4b03ed7ee119ffcd84a77e108045.mugqic.done'
mkdir -p report && \
cp \
  /home/efournie/genpipes/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.8bfd4b03ed7ee119ffcd84a77e108045.mugqic.done
)
picard_mark_duplicates_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: metrics
#-------------------------------------------------------------------------------
STEP=metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: metrics_1_JOB_ID: metrics.flagstat
#-------------------------------------------------------------------------------
JOB_NAME=metrics.flagstat
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/metrics/metrics.flagstat.c050d8d5900deabe5d5613b90474d2a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.flagstat.c050d8d5900deabe5d5613b90474d2a8.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools flagstat \
  alignment/P01-OCT-H/P01-OCT-H.sorted.dup.bam \
  > alignment/P01-OCT-H/P01-OCT-H.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P01-SF-C/P01-SF-C.sorted.dup.bam \
  > alignment/P01-SF-C/P01-SF-C.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.bam \
  > alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.bam \
  > alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.bam \
  > alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.bam \
  > alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.bam \
  > alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.bam \
  > alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/PZ-HPV7/PZ-HPV7.sorted.dup.bam \
  > alignment/PZ-HPV7/PZ-HPV7.sorted.dup.bam.flagstat
metrics.flagstat.c050d8d5900deabe5d5613b90474d2a8.mugqic.done
)
metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: metrics_2_JOB_ID: metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=metrics_report
JOB_DEPENDENCIES=$metrics_1_JOB_ID
JOB_DONE=job_output/metrics/metrics_report.6c76f83fae683fdb830a42ff06bd3832.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics_report.6c76f83fae683fdb830a42ff06bd3832.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
for sample in P01-OCT-H P01-SF-C P056-FRZ6-5p P059-FRZ5-5p P62-FRZ3-3p P081-FRZ5-3p P092-FRZ3-4p P096-FRZ7-4p PZ-HPV7
do
  flagstat_file=alignment/$sample/$sample.sorted.dup.bam.flagstat
  echo -e "$sample	`grep -P '^\d+ \+ \d+ mapped' $flagstat_file | grep -Po '^\d+'`	`grep -P '^\d+ \+ \d+ duplicate' $flagstat_file | grep -Po '^\d+'`"
done | \
awk -F"	" '{OFS="	"; print $0, $3 / $2 * 100}' | sed '1iSample	Aligned Filtered Reads	Duplicate Reads	Duplicate %' \
  > metrics/SampleMetrics.stats && \
mkdir -p report && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F "	" 'FNR==NR{trim_line[$1]=$0; surviving[$1]=$3; next}{OFS="	"; if ($1=="Sample") {print trim_line[$1], $2, "Aligned Filtered %", $3, $4} else {print trim_line[$1], $2, $2 / surviving[$1] * 100, $3, $4}}' metrics/trimSampleTable.tsv metrics/SampleMetrics.stats \
  > report/trimMemSampleTable.tsv
else
  cp metrics/SampleMetrics.stats report/trimMemSampleTable.tsv
fi && \
trim_mem_sample_table=`if [[ -f metrics/trimSampleTable.tsv ]] ; then LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8)}}' report/trimMemSampleTable.tsv ; else LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4)}}' report/trimMemSampleTable.tsv ; fi` && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/ChipSeq.metrics.md \
  --variable trim_mem_sample_table="$trim_mem_sample_table" \
  /home/efournie/genpipes/bfx/report/ChipSeq.metrics.md \
  > report/ChipSeq.metrics.md

metrics_report.6c76f83fae683fdb830a42ff06bd3832.mugqic.done
)
metrics_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: homer_make_tag_directory
#-------------------------------------------------------------------------------
STEP=homer_make_tag_directory
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_1_JOB_ID: homer_make_tag_directory.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P01-OCT-H
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P01-OCT-H.f777110deb7e43d1f8d92887f3b46669.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P01-OCT-H.f777110deb7e43d1f8d92887f3b46669.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P01-OCT-H \
            alignment/P01-OCT-H/P01-OCT-H.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P01-OCT-H.f777110deb7e43d1f8d92887f3b46669.mugqic.done
)
homer_make_tag_directory_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_2_JOB_ID: homer_make_tag_directory.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P01-SF-C
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P01-SF-C.8bb6c846e77f6230dfb92e70e6c11b65.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P01-SF-C.8bb6c846e77f6230dfb92e70e6c11b65.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P01-SF-C \
            alignment/P01-SF-C/P01-SF-C.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P01-SF-C.8bb6c846e77f6230dfb92e70e6c11b65.mugqic.done
)
homer_make_tag_directory_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_3_JOB_ID: homer_make_tag_directory.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P056-FRZ6-5p
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P056-FRZ6-5p.82e426b7f2397ae7714821a0d4eca643.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P056-FRZ6-5p.82e426b7f2397ae7714821a0d4eca643.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P056-FRZ6-5p \
            alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P056-FRZ6-5p.82e426b7f2397ae7714821a0d4eca643.mugqic.done
)
homer_make_tag_directory_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_4_JOB_ID: homer_make_tag_directory.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P059-FRZ5-5p
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P059-FRZ5-5p.55f56d49a3683c511c39a2ea17bc1aaa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P059-FRZ5-5p.55f56d49a3683c511c39a2ea17bc1aaa.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P059-FRZ5-5p \
            alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P059-FRZ5-5p.55f56d49a3683c511c39a2ea17bc1aaa.mugqic.done
)
homer_make_tag_directory_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_5_JOB_ID: homer_make_tag_directory.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P62-FRZ3-3p
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P62-FRZ3-3p.aed92d08cdd8acd58dc034e67341461b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P62-FRZ3-3p.aed92d08cdd8acd58dc034e67341461b.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P62-FRZ3-3p \
            alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P62-FRZ3-3p.aed92d08cdd8acd58dc034e67341461b.mugqic.done
)
homer_make_tag_directory_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_6_JOB_ID: homer_make_tag_directory.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P081-FRZ5-3p
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P081-FRZ5-3p.38e9cac090f6329d10d95b42bf6341be.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P081-FRZ5-3p.38e9cac090f6329d10d95b42bf6341be.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P081-FRZ5-3p \
            alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P081-FRZ5-3p.38e9cac090f6329d10d95b42bf6341be.mugqic.done
)
homer_make_tag_directory_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_7_JOB_ID: homer_make_tag_directory.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P092-FRZ3-4p
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P092-FRZ3-4p.225d7fbf7ac43425267fbaf6941cddcb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P092-FRZ3-4p.225d7fbf7ac43425267fbaf6941cddcb.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P092-FRZ3-4p \
            alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P092-FRZ3-4p.225d7fbf7ac43425267fbaf6941cddcb.mugqic.done
)
homer_make_tag_directory_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_8_JOB_ID: homer_make_tag_directory.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.P096-FRZ7-4p
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.P096-FRZ7-4p.44232876cb75bd77e95bd9d540fcfae4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.P096-FRZ7-4p.44232876cb75bd77e95bd9d540fcfae4.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/P096-FRZ7-4p \
            alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.P096-FRZ7-4p.44232876cb75bd77e95bd9d540fcfae4.mugqic.done
)
homer_make_tag_directory_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_9_JOB_ID: homer_make_tag_directory.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.PZ-HPV7
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.PZ-HPV7.39c0ec79988c7d2f5a7ca1396ff0867f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.PZ-HPV7.39c0ec79988c7d2f5a7ca1396ff0867f.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 mugqic/samtools/1.3.1 && \
makeTagDirectory tags/PZ-HPV7 \
            alignment/PZ-HPV7/PZ-HPV7.sorted.dup.bam \
            -genome hg38 \
            -checkGC \
 
homer_make_tag_directory.PZ-HPV7.39c0ec79988c7d2f5a7ca1396ff0867f.mugqic.done
)
homer_make_tag_directory_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_tag_directory_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: qc_metrics
#-------------------------------------------------------------------------------
STEP=qc_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: qc_metrics_1_JOB_ID: qc_plots_R
#-------------------------------------------------------------------------------
JOB_NAME=qc_plots_R
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID:$homer_make_tag_directory_2_JOB_ID:$homer_make_tag_directory_3_JOB_ID:$homer_make_tag_directory_4_JOB_ID:$homer_make_tag_directory_5_JOB_ID:$homer_make_tag_directory_6_JOB_ID:$homer_make_tag_directory_7_JOB_ID:$homer_make_tag_directory_8_JOB_ID:$homer_make_tag_directory_9_JOB_ID
JOB_DONE=job_output/qc_metrics/qc_plots_R.3bf62fbde8a6a8ed6cde02e3c3da83a2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'qc_plots_R.3bf62fbde8a6a8ed6cde02e3c3da83a2.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p graphs && \
Rscript $R_TOOLS/chipSeqGenerateQCMetrics.R \
  ../../raw/design.txt \
  /scratch/efournie/ATAC-seq/output/pipeline && \
cp /home/efournie/genpipes/bfx/report/ChipSeq.qc_metrics.md report/ChipSeq.qc_metrics.md && \
for sample in P01-OCT-H P01-SF-C P056-FRZ6-5p P059-FRZ5-5p P62-FRZ3-3p P081-FRZ5-3p P092-FRZ3-4p P096-FRZ7-4p PZ-HPV7
do
  cp --parents graphs/${sample}_QC_Metrics.ps report/
  convert -rotate 90 graphs/${sample}_QC_Metrics.ps report/graphs/${sample}_QC_Metrics.png
  echo -e "----

![QC Metrics for Sample $sample ([download high-res image](graphs/${sample}_QC_Metrics.ps))](graphs/${sample}_QC_Metrics.png)
" \
  >> report/ChipSeq.qc_metrics.md
done
qc_plots_R.3bf62fbde8a6a8ed6cde02e3c3da83a2.mugqic.done
)
qc_metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"qc_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/chipseq/chipseq.base.ini,/home/efournie/genpipes/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini\" \
  -s \"qc_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/ATAC-seq/output/pipeline/json/P01-OCT-H.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P01-SF-C.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P056-FRZ6-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P059-FRZ5-5p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P62-FRZ3-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P081-FRZ5-3p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P092-FRZ3-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/P096-FRZ7-4p.json,/scratch/efournie/ATAC-seq/output/pipeline/json/PZ-HPV7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$qc_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: homer_make_ucsc_file
#-------------------------------------------------------------------------------
STEP=homer_make_ucsc_file
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_1_JOB_ID: homer_make_ucsc_file.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P01-OCT-H
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P01-OCT-H.0dd5fa32914eb6035169b0bcca2dacfc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P01-OCT-H.0dd5fa32914eb6035169b0bcca2dacfc.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P01-OCT-H && \
makeUCSCfile \
        tags/P01-OCT-H > tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph && \
        gzip -c tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph > tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.gz
homer_make_ucsc_file.P01-OCT-H.0dd5fa32914eb6035169b0bcca2dacfc.mugqic.done
)
homer_make_ucsc_file_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_2_JOB_ID: homer_make_ucsc_file_bigWig.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P01-OCT-H
JOB_DEPENDENCIES=$homer_make_ucsc_file_1_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P01-OCT-H.2b51687847fa51307edd1fbd1f26f689.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P01-OCT-H.2b51687847fa51307edd1fbd1f26f689.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P01-OCT-H/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph | head -n 1 > tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.head.tmp && \
cat tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.body.tmp && \
cat tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.head.tmp tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.body.tmp > tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.sorted && \
rm tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.head.tmp tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P01-OCT-H/P01-OCT-H.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P01-OCT-H/bigWig/P01-OCT-H.bw
homer_make_ucsc_file_bigWig.P01-OCT-H.2b51687847fa51307edd1fbd1f26f689.mugqic.done
)
homer_make_ucsc_file_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_3_JOB_ID: homer_make_ucsc_file.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P01-SF-C
JOB_DEPENDENCIES=$homer_make_tag_directory_2_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P01-SF-C.8f4300f1143adbea308ac55cf79104ec.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P01-SF-C.8f4300f1143adbea308ac55cf79104ec.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P01-SF-C && \
makeUCSCfile \
        tags/P01-SF-C > tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph && \
        gzip -c tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph > tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.gz
homer_make_ucsc_file.P01-SF-C.8f4300f1143adbea308ac55cf79104ec.mugqic.done
)
homer_make_ucsc_file_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_4_JOB_ID: homer_make_ucsc_file_bigWig.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P01-SF-C
JOB_DEPENDENCIES=$homer_make_ucsc_file_3_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P01-SF-C.a1ffbefb01c936793e44ebbfbc05a7bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P01-SF-C.a1ffbefb01c936793e44ebbfbc05a7bd.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P01-SF-C/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph | head -n 1 > tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.head.tmp && \
cat tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.body.tmp && \
cat tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.head.tmp tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.body.tmp > tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.sorted && \
rm tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.head.tmp tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P01-SF-C/P01-SF-C.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P01-SF-C/bigWig/P01-SF-C.bw
homer_make_ucsc_file_bigWig.P01-SF-C.a1ffbefb01c936793e44ebbfbc05a7bd.mugqic.done
)
homer_make_ucsc_file_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_5_JOB_ID: homer_make_ucsc_file.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P056-FRZ6-5p
JOB_DEPENDENCIES=$homer_make_tag_directory_3_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P056-FRZ6-5p.a15389a0359726ef6789b6f2d611b21a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P056-FRZ6-5p.a15389a0359726ef6789b6f2d611b21a.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P056-FRZ6-5p && \
makeUCSCfile \
        tags/P056-FRZ6-5p > tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph && \
        gzip -c tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph > tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.gz
homer_make_ucsc_file.P056-FRZ6-5p.a15389a0359726ef6789b6f2d611b21a.mugqic.done
)
homer_make_ucsc_file_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_6_JOB_ID: homer_make_ucsc_file_bigWig.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P056-FRZ6-5p
JOB_DEPENDENCIES=$homer_make_ucsc_file_5_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P056-FRZ6-5p.85fb634cf082b13aa316ede0d38b3cf3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P056-FRZ6-5p.85fb634cf082b13aa316ede0d38b3cf3.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P056-FRZ6-5p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph | head -n 1 > tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.head.tmp && \
cat tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.body.tmp && \
cat tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.head.tmp tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.body.tmp > tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.sorted && \
rm tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.head.tmp tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P056-FRZ6-5p/P056-FRZ6-5p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P056-FRZ6-5p/bigWig/P056-FRZ6-5p.bw
homer_make_ucsc_file_bigWig.P056-FRZ6-5p.85fb634cf082b13aa316ede0d38b3cf3.mugqic.done
)
homer_make_ucsc_file_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_7_JOB_ID: homer_make_ucsc_file.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P059-FRZ5-5p
JOB_DEPENDENCIES=$homer_make_tag_directory_4_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P059-FRZ5-5p.1faebc791c7b40233d0c5fdab7fa51a4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P059-FRZ5-5p.1faebc791c7b40233d0c5fdab7fa51a4.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P059-FRZ5-5p && \
makeUCSCfile \
        tags/P059-FRZ5-5p > tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph && \
        gzip -c tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph > tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.gz
homer_make_ucsc_file.P059-FRZ5-5p.1faebc791c7b40233d0c5fdab7fa51a4.mugqic.done
)
homer_make_ucsc_file_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_8_JOB_ID: homer_make_ucsc_file_bigWig.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P059-FRZ5-5p
JOB_DEPENDENCIES=$homer_make_ucsc_file_7_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P059-FRZ5-5p.f2bd5b961e4ea4a46f82576b09eb9d87.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P059-FRZ5-5p.f2bd5b961e4ea4a46f82576b09eb9d87.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P059-FRZ5-5p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph | head -n 1 > tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.head.tmp && \
cat tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.body.tmp && \
cat tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.head.tmp tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.body.tmp > tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.sorted && \
rm tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.head.tmp tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P059-FRZ5-5p/P059-FRZ5-5p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P059-FRZ5-5p/bigWig/P059-FRZ5-5p.bw
homer_make_ucsc_file_bigWig.P059-FRZ5-5p.f2bd5b961e4ea4a46f82576b09eb9d87.mugqic.done
)
homer_make_ucsc_file_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_9_JOB_ID: homer_make_ucsc_file.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P62-FRZ3-3p
JOB_DEPENDENCIES=$homer_make_tag_directory_5_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P62-FRZ3-3p.a69f38dfac638ad597c752b76c73e514.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P62-FRZ3-3p.a69f38dfac638ad597c752b76c73e514.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P62-FRZ3-3p && \
makeUCSCfile \
        tags/P62-FRZ3-3p > tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph && \
        gzip -c tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph > tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.gz
homer_make_ucsc_file.P62-FRZ3-3p.a69f38dfac638ad597c752b76c73e514.mugqic.done
)
homer_make_ucsc_file_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_10_JOB_ID: homer_make_ucsc_file_bigWig.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P62-FRZ3-3p
JOB_DEPENDENCIES=$homer_make_ucsc_file_9_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P62-FRZ3-3p.b36815b538c74f359f738d6e3a576ca9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P62-FRZ3-3p.b36815b538c74f359f738d6e3a576ca9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P62-FRZ3-3p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph | head -n 1 > tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.head.tmp && \
cat tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.body.tmp && \
cat tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.head.tmp tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.body.tmp > tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.sorted && \
rm tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.head.tmp tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P62-FRZ3-3p/P62-FRZ3-3p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P62-FRZ3-3p/bigWig/P62-FRZ3-3p.bw
homer_make_ucsc_file_bigWig.P62-FRZ3-3p.b36815b538c74f359f738d6e3a576ca9.mugqic.done
)
homer_make_ucsc_file_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_11_JOB_ID: homer_make_ucsc_file.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P081-FRZ5-3p
JOB_DEPENDENCIES=$homer_make_tag_directory_6_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P081-FRZ5-3p.2d9a0d5f4c4ae117922f0681b11bbd97.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P081-FRZ5-3p.2d9a0d5f4c4ae117922f0681b11bbd97.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P081-FRZ5-3p && \
makeUCSCfile \
        tags/P081-FRZ5-3p > tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph && \
        gzip -c tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph > tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.gz
homer_make_ucsc_file.P081-FRZ5-3p.2d9a0d5f4c4ae117922f0681b11bbd97.mugqic.done
)
homer_make_ucsc_file_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_12_JOB_ID: homer_make_ucsc_file_bigWig.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P081-FRZ5-3p
JOB_DEPENDENCIES=$homer_make_ucsc_file_11_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P081-FRZ5-3p.5b89216e2f1b3b6907156189e1329c82.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P081-FRZ5-3p.5b89216e2f1b3b6907156189e1329c82.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P081-FRZ5-3p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph | head -n 1 > tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.head.tmp && \
cat tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.body.tmp && \
cat tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.head.tmp tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.body.tmp > tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.sorted && \
rm tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.head.tmp tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P081-FRZ5-3p/P081-FRZ5-3p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P081-FRZ5-3p/bigWig/P081-FRZ5-3p.bw
homer_make_ucsc_file_bigWig.P081-FRZ5-3p.5b89216e2f1b3b6907156189e1329c82.mugqic.done
)
homer_make_ucsc_file_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_13_JOB_ID: homer_make_ucsc_file.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P092-FRZ3-4p
JOB_DEPENDENCIES=$homer_make_tag_directory_7_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P092-FRZ3-4p.fe089f2d03ddbc6c321b338296c32a0b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P092-FRZ3-4p.fe089f2d03ddbc6c321b338296c32a0b.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P092-FRZ3-4p && \
makeUCSCfile \
        tags/P092-FRZ3-4p > tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph && \
        gzip -c tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph > tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.gz
homer_make_ucsc_file.P092-FRZ3-4p.fe089f2d03ddbc6c321b338296c32a0b.mugqic.done
)
homer_make_ucsc_file_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_14_JOB_ID: homer_make_ucsc_file_bigWig.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P092-FRZ3-4p
JOB_DEPENDENCIES=$homer_make_ucsc_file_13_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P092-FRZ3-4p.81cd901fc7304bf844c0966cbae59a3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P092-FRZ3-4p.81cd901fc7304bf844c0966cbae59a3a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P092-FRZ3-4p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph | head -n 1 > tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.head.tmp && \
cat tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.body.tmp && \
cat tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.head.tmp tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.body.tmp > tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.sorted && \
rm tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.head.tmp tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P092-FRZ3-4p/P092-FRZ3-4p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P092-FRZ3-4p/bigWig/P092-FRZ3-4p.bw
homer_make_ucsc_file_bigWig.P092-FRZ3-4p.81cd901fc7304bf844c0966cbae59a3a.mugqic.done
)
homer_make_ucsc_file_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_15_JOB_ID: homer_make_ucsc_file.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.P096-FRZ7-4p
JOB_DEPENDENCIES=$homer_make_tag_directory_8_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.P096-FRZ7-4p.19c13c6cdc4d6cf206c930dccdaf5187.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.P096-FRZ7-4p.19c13c6cdc4d6cf206c930dccdaf5187.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/P096-FRZ7-4p && \
makeUCSCfile \
        tags/P096-FRZ7-4p > tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph && \
        gzip -c tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph > tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.gz
homer_make_ucsc_file.P096-FRZ7-4p.19c13c6cdc4d6cf206c930dccdaf5187.mugqic.done
)
homer_make_ucsc_file_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_16_JOB_ID: homer_make_ucsc_file_bigWig.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.P096-FRZ7-4p
JOB_DEPENDENCIES=$homer_make_ucsc_file_15_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.P096-FRZ7-4p.79b37e11f880e8b686445bd77f2eefa4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.P096-FRZ7-4p.79b37e11f880e8b686445bd77f2eefa4.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/P096-FRZ7-4p/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph | head -n 1 > tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.head.tmp && \
cat tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.body.tmp && \
cat tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.head.tmp tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.body.tmp > tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.sorted && \
rm tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.head.tmp tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/P096-FRZ7-4p/P096-FRZ7-4p.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/P096-FRZ7-4p/bigWig/P096-FRZ7-4p.bw
homer_make_ucsc_file_bigWig.P096-FRZ7-4p.79b37e11f880e8b686445bd77f2eefa4.mugqic.done
)
homer_make_ucsc_file_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_17_JOB_ID: homer_make_ucsc_file.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.PZ-HPV7
JOB_DEPENDENCIES=$homer_make_tag_directory_9_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.PZ-HPV7.c9b6b2f6d14315311e3e34fa719ec3c1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.PZ-HPV7.c9b6b2f6d14315311e3e34fa719ec3c1.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.9.1 && \
mkdir -p tracks/PZ-HPV7 && \
makeUCSCfile \
        tags/PZ-HPV7 > tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph && \
        gzip -c tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph > tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.gz
homer_make_ucsc_file.PZ-HPV7.c9b6b2f6d14315311e3e34fa719ec3c1.mugqic.done
)
homer_make_ucsc_file_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_18_JOB_ID: homer_make_ucsc_file_bigWig.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_bigWig.PZ-HPV7
JOB_DEPENDENCIES=$homer_make_ucsc_file_17_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_bigWig.PZ-HPV7.9a2908def9b4db5f3ca14a4582393862.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_bigWig.PZ-HPV7.9a2908def9b4db5f3ca14a4582393862.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/PZ-HPV7/bigWig && \
export TMPDIR=${SLURM_TMPDIR} && \
cat tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph | head -n 1 > tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.head.tmp && \
cat tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph | awk ' NR > 1 ' | sort  --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.body.tmp && \
cat tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.head.tmp tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.body.tmp > tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.sorted && \
rm tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.head.tmp tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.body.tmp && \
bedGraphToBigWig \
  tracks/PZ-HPV7/PZ-HPV7.ucsc.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  tracks/PZ-HPV7/bigWig/PZ-HPV7.bw
homer_make_ucsc_file_bigWig.PZ-HPV7.9a2908def9b4db5f3ca14a4582393862.mugqic.done
)
homer_make_ucsc_file_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_19_JOB_ID: homer_make_ucsc_file_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_report
JOB_DEPENDENCIES=$homer_make_ucsc_file_17_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_report.90b1cb89146cadaf805ea66244074321.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_report.90b1cb89146cadaf805ea66244074321.mugqic.done'
mkdir -p report && \
zip -r report/tracks.zip tracks/*/*.ucsc.bedGraph.gz && \
cp /home/efournie/genpipes/bfx/report/ChipSeq.homer_make_ucsc_file.md report/
homer_make_ucsc_file_report.90b1cb89146cadaf805ea66244074321.mugqic.done
)
homer_make_ucsc_file_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$homer_make_ucsc_file_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P01-OCT-H
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P01-OCT-H.e6c7970288a54fd2baca361858d5ca7c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P01-OCT-H.e6c7970288a54fd2baca361858d5ca7c.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P01-OCT-H && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P01-OCT-H/P01-OCT-H.sorted.dup.bam \
  --nolambda \
  --name peak_call/P01-OCT-H/P01-OCT-H \
  >& peak_call/P01-OCT-H/P01-OCT-H.diag.macs.out
macs2_callpeak.P01-OCT-H.e6c7970288a54fd2baca361858d5ca7c.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak_bigBed.P01-OCT-H
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P01-OCT-H
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P01-OCT-H.4c5e4cc7cf0a86ea74cb3237b9ba1cf5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P01-OCT-H.4c5e4cc7cf0a86ea74cb3237b9ba1cf5.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P01-OCT-H/P01-OCT-H_peaks.narrowPeak > peak_call/P01-OCT-H/P01-OCT-H_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P01-OCT-H/P01-OCT-H_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P01-OCT-H/P01-OCT-H_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P01-OCT-H.4c5e4cc7cf0a86ea74cb3237b9ba1cf5.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P01-SF-C
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P01-SF-C.adbd0fb73c6914d8102cb5ca9980463a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P01-SF-C.adbd0fb73c6914d8102cb5ca9980463a.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P01-SF-C && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P01-SF-C/P01-SF-C.sorted.dup.bam \
  --nolambda \
  --name peak_call/P01-SF-C/P01-SF-C \
  >& peak_call/P01-SF-C/P01-SF-C.diag.macs.out
macs2_callpeak.P01-SF-C.adbd0fb73c6914d8102cb5ca9980463a.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak_bigBed.P01-SF-C
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P01-SF-C
JOB_DEPENDENCIES=$macs2_callpeak_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P01-SF-C.4298298bd5d1bd021068b05babd83a33.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P01-SF-C.4298298bd5d1bd021068b05babd83a33.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P01-SF-C/P01-SF-C_peaks.narrowPeak > peak_call/P01-SF-C/P01-SF-C_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P01-SF-C/P01-SF-C_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P01-SF-C/P01-SF-C_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P01-SF-C.4298298bd5d1bd021068b05babd83a33.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P056-FRZ6-5p
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P056-FRZ6-5p.0a96bcf4c9de80f2b8fdc85049ecb6f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P056-FRZ6-5p.0a96bcf4c9de80f2b8fdc85049ecb6f9.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P056-FRZ6-5p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P056-FRZ6-5p/P056-FRZ6-5p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P056-FRZ6-5p/P056-FRZ6-5p \
  >& peak_call/P056-FRZ6-5p/P056-FRZ6-5p.diag.macs.out
macs2_callpeak.P056-FRZ6-5p.0a96bcf4c9de80f2b8fdc85049ecb6f9.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_bigBed.P056-FRZ6-5p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P056-FRZ6-5p
JOB_DEPENDENCIES=$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P056-FRZ6-5p.0284ca30c47e95181d1e90f4539c8d40.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P056-FRZ6-5p.0284ca30c47e95181d1e90f4539c8d40.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P056-FRZ6-5p/P056-FRZ6-5p_peaks.narrowPeak > peak_call/P056-FRZ6-5p/P056-FRZ6-5p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P056-FRZ6-5p/P056-FRZ6-5p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P056-FRZ6-5p/P056-FRZ6-5p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P056-FRZ6-5p.0284ca30c47e95181d1e90f4539c8d40.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_7_JOB_ID: macs2_callpeak.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P059-FRZ5-5p
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P059-FRZ5-5p.43ed72b991c7f0f489b6161e9a800255.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P059-FRZ5-5p.43ed72b991c7f0f489b6161e9a800255.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P059-FRZ5-5p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P059-FRZ5-5p/P059-FRZ5-5p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P059-FRZ5-5p/P059-FRZ5-5p \
  >& peak_call/P059-FRZ5-5p/P059-FRZ5-5p.diag.macs.out
macs2_callpeak.P059-FRZ5-5p.43ed72b991c7f0f489b6161e9a800255.mugqic.done
)
macs2_callpeak_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_8_JOB_ID: macs2_callpeak_bigBed.P059-FRZ5-5p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P059-FRZ5-5p
JOB_DEPENDENCIES=$macs2_callpeak_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P059-FRZ5-5p.3e0e6319942e2a30f0ee239e44140271.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P059-FRZ5-5p.3e0e6319942e2a30f0ee239e44140271.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P059-FRZ5-5p/P059-FRZ5-5p_peaks.narrowPeak > peak_call/P059-FRZ5-5p/P059-FRZ5-5p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P059-FRZ5-5p/P059-FRZ5-5p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P059-FRZ5-5p/P059-FRZ5-5p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P059-FRZ5-5p.3e0e6319942e2a30f0ee239e44140271.mugqic.done
)
macs2_callpeak_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_9_JOB_ID: macs2_callpeak.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P62-FRZ3-3p
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P62-FRZ3-3p.6519459948175e901321d54e42e00d3e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P62-FRZ3-3p.6519459948175e901321d54e42e00d3e.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P62-FRZ3-3p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P62-FRZ3-3p/P62-FRZ3-3p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P62-FRZ3-3p/P62-FRZ3-3p \
  >& peak_call/P62-FRZ3-3p/P62-FRZ3-3p.diag.macs.out
macs2_callpeak.P62-FRZ3-3p.6519459948175e901321d54e42e00d3e.mugqic.done
)
macs2_callpeak_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_10_JOB_ID: macs2_callpeak_bigBed.P62-FRZ3-3p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P62-FRZ3-3p
JOB_DEPENDENCIES=$macs2_callpeak_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P62-FRZ3-3p.2cab09607ac0c7fec913b2385787097d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P62-FRZ3-3p.2cab09607ac0c7fec913b2385787097d.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P62-FRZ3-3p/P62-FRZ3-3p_peaks.narrowPeak > peak_call/P62-FRZ3-3p/P62-FRZ3-3p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P62-FRZ3-3p/P62-FRZ3-3p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P62-FRZ3-3p/P62-FRZ3-3p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P62-FRZ3-3p.2cab09607ac0c7fec913b2385787097d.mugqic.done
)
macs2_callpeak_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_11_JOB_ID: macs2_callpeak.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P081-FRZ5-3p
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P081-FRZ5-3p.928f7743a3756cd6ad0548bdca5f9662.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P081-FRZ5-3p.928f7743a3756cd6ad0548bdca5f9662.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P081-FRZ5-3p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P081-FRZ5-3p/P081-FRZ5-3p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P081-FRZ5-3p/P081-FRZ5-3p \
  >& peak_call/P081-FRZ5-3p/P081-FRZ5-3p.diag.macs.out
macs2_callpeak.P081-FRZ5-3p.928f7743a3756cd6ad0548bdca5f9662.mugqic.done
)
macs2_callpeak_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_12_JOB_ID: macs2_callpeak_bigBed.P081-FRZ5-3p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P081-FRZ5-3p
JOB_DEPENDENCIES=$macs2_callpeak_11_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P081-FRZ5-3p.cec672b11bbc8f86a0d273e0e5bbd2a4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P081-FRZ5-3p.cec672b11bbc8f86a0d273e0e5bbd2a4.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P081-FRZ5-3p/P081-FRZ5-3p_peaks.narrowPeak > peak_call/P081-FRZ5-3p/P081-FRZ5-3p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P081-FRZ5-3p/P081-FRZ5-3p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P081-FRZ5-3p/P081-FRZ5-3p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P081-FRZ5-3p.cec672b11bbc8f86a0d273e0e5bbd2a4.mugqic.done
)
macs2_callpeak_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_13_JOB_ID: macs2_callpeak.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P092-FRZ3-4p
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P092-FRZ3-4p.32e0d96917f18c779d8c49cdf54b6cce.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P092-FRZ3-4p.32e0d96917f18c779d8c49cdf54b6cce.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P092-FRZ3-4p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P092-FRZ3-4p/P092-FRZ3-4p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P092-FRZ3-4p/P092-FRZ3-4p \
  >& peak_call/P092-FRZ3-4p/P092-FRZ3-4p.diag.macs.out
macs2_callpeak.P092-FRZ3-4p.32e0d96917f18c779d8c49cdf54b6cce.mugqic.done
)
macs2_callpeak_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_14_JOB_ID: macs2_callpeak_bigBed.P092-FRZ3-4p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P092-FRZ3-4p
JOB_DEPENDENCIES=$macs2_callpeak_13_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P092-FRZ3-4p.70d4eac4f6ecba31a1c2595cadf1bcc1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P092-FRZ3-4p.70d4eac4f6ecba31a1c2595cadf1bcc1.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P092-FRZ3-4p/P092-FRZ3-4p_peaks.narrowPeak > peak_call/P092-FRZ3-4p/P092-FRZ3-4p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P092-FRZ3-4p/P092-FRZ3-4p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P092-FRZ3-4p/P092-FRZ3-4p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P092-FRZ3-4p.70d4eac4f6ecba31a1c2595cadf1bcc1.mugqic.done
)
macs2_callpeak_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_15_JOB_ID: macs2_callpeak.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.P096-FRZ7-4p
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.P096-FRZ7-4p.9230e040f9c2be6eaf93ae81bb70aa2f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.P096-FRZ7-4p.9230e040f9c2be6eaf93ae81bb70aa2f.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/P096-FRZ7-4p && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/P096-FRZ7-4p/P096-FRZ7-4p.sorted.dup.bam \
  --nolambda \
  --name peak_call/P096-FRZ7-4p/P096-FRZ7-4p \
  >& peak_call/P096-FRZ7-4p/P096-FRZ7-4p.diag.macs.out
macs2_callpeak.P096-FRZ7-4p.9230e040f9c2be6eaf93ae81bb70aa2f.mugqic.done
)
macs2_callpeak_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_16_JOB_ID: macs2_callpeak_bigBed.P096-FRZ7-4p
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.P096-FRZ7-4p
JOB_DEPENDENCIES=$macs2_callpeak_15_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.P096-FRZ7-4p.fd425362c3ada6c08a8412f010aac087.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.P096-FRZ7-4p.fd425362c3ada6c08a8412f010aac087.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/P096-FRZ7-4p/P096-FRZ7-4p_peaks.narrowPeak > peak_call/P096-FRZ7-4p/P096-FRZ7-4p_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/P096-FRZ7-4p/P096-FRZ7-4p_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/P096-FRZ7-4p/P096-FRZ7-4p_peaks.narrowPeak.bb
macs2_callpeak_bigBed.P096-FRZ7-4p.fd425362c3ada6c08a8412f010aac087.mugqic.done
)
macs2_callpeak_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_17_JOB_ID: macs2_callpeak.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.PZ-HPV7
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.PZ-HPV7.65fd3f328671786ccf2a766049ae97ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.PZ-HPV7.65fd3f328671786ccf2a766049ae97ba.mugqic.done'
module load mugqic/python/2.7.13 mugqic/MACS2/2.1.1.20160309 && \
mkdir -p peak_call/PZ-HPV7 && \
macs2 callpeak --format BAM --fix-bimodal \
  --tempdir ${SLURM_TMPDIR} \
  --gsize 2479938032.8 \
  --treatment \
  alignment/PZ-HPV7/PZ-HPV7.sorted.dup.bam \
  --nolambda \
  --name peak_call/PZ-HPV7/PZ-HPV7 \
  >& peak_call/PZ-HPV7/PZ-HPV7.diag.macs.out
macs2_callpeak.PZ-HPV7.65fd3f328671786ccf2a766049ae97ba.mugqic.done
)
macs2_callpeak_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_18_JOB_ID: macs2_callpeak_bigBed.PZ-HPV7
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_bigBed.PZ-HPV7
JOB_DEPENDENCIES=$macs2_callpeak_17_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_bigBed.PZ-HPV7.ad06ce9699f15ab5bb5dc166f3f3bd40.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_bigBed.PZ-HPV7.ad06ce9699f15ab5bb5dc166f3f3bd40.mugqic.done'
module load mugqic/ucsc/v346 && \
awk ' {if ($9 > 1000) {$9 = 1000} ; printf( "%s\t%s\t%s\t%s\t%0.f\n", $1,$2,$3,$4,$9)} ' peak_call/PZ-HPV7/PZ-HPV7_peaks.narrowPeak > peak_call/PZ-HPV7/PZ-HPV7_peaks.narrowPeak.bed && \
bedToBigBed \
  peak_call/PZ-HPV7/PZ-HPV7_peaks.narrowPeak.bed \
  /project/6007512/C3G/analyste_dev/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa.fai \
  peak_call/PZ-HPV7/PZ-HPV7_peaks.narrowPeak.bb
macs2_callpeak_bigBed.PZ-HPV7.ad06ce9699f15ab5bb5dc166f3f3bd40.mugqic.done
)
macs2_callpeak_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_19_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_5_JOB_ID:$macs2_callpeak_7_JOB_ID:$macs2_callpeak_9_JOB_ID:$macs2_callpeak_11_JOB_ID:$macs2_callpeak_13_JOB_ID:$macs2_callpeak_15_JOB_ID:$macs2_callpeak_17_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.f4947cbe0bf377727839425bdbe8b203.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.f4947cbe0bf377727839425bdbe8b203.mugqic.done'
mkdir -p report && \
cp /home/efournie/genpipes/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in P01-OCT-H P01-SF-C P056-FRZ6-5p P059-FRZ5-5p P62-FRZ3-3p P081-FRZ5-3p P092-FRZ3-4p P096-FRZ7-4p PZ-HPV7
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.f4947cbe0bf377727839425bdbe8b203.mugqic.done
)
macs2_callpeak_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$macs2_callpeak_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,metrics,homer_make_tag_directory,qc_metrics,homer_make_ucsc_file,macs2_callpeak&samples=9&AnonymizedList=55ac5543433132cc774afe914b119a04,5d15839bb27c318450863df2f7d3611a,551dc4b000ef7ca5447bbfca18a1f1a9,d42cddd72714c9738962db9d98e2e229,98010e2850e550243a3152fe4abf589e,a910e030caf787159f2b3a53de447749,ae4569963209a05319236607f8585283,39b7316d7ca22dbd13cd71a1adee397b,290c529990939447faa092aac11bd82d" --quiet --output-document=/dev/null

